Car API Frontend
=====

A sample frontend to the car inventory API written in Angular JS. The 
application makes it possible to use all functionality provided by the API,
and provides a simple responsive layout for the data.
