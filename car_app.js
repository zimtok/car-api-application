var app = angular.module('carApp', []);


//Put a few helper methods in the carUtil service
app.service('carUtil', function(){
    var apiUrl = 'http://localhost:8000';
    
    this.getApiUrl = function(){return apiUrl;};
    
    this.stringToDate = function (dateStr){
        var parts = dateStr.split("-");
        var date = new Date(parseInt(parts[2], 10),
                  parseInt(parts[1], 10) - 1,
                  parseInt(parts[0], 10));
        return date;
    };
    this.dateToString = function(dateObj){
        return dateObj.getDate() + '-'
        + (dateObj.getMonth() + 1) + '-'
        + dateObj.getFullYear();
    }

});

//Main page
app.controller('mainController', function($scope, $http, $window, carUtil) {
    $http.get(carUtil.getApiUrl() + '/api').
    then(function(response) {
        $scope.cars = response.data;
    });
    $scope.deleteCar = function(index){
        var car = $scope.cars[index];
        $scope.cars.splice(index, 1);
        console.log(car);
        $http.delete(carUtil.getApiUrl() + '/api/' + car.id).
        then(function(response) {
            $window.alert("Delete successful");
        });
    };
    
    $scope.carDetails = function(id){
        $window.sessionStorage.setItem('carSelection', id);
        $window.location.href = 'car_app_details.html';
    };
    
    $scope.editCar = function(id){
        $window.sessionStorage.setItem('carSelection', id);
        $window.location.href = 'car_app_edit.html';
    };
    
    $scope.createCar = function(){
        $window.location.href = 'car_app_create.html';
    };
});

//Adding a new car
app.controller('createController', function($scope, $http, $window, carUtil) {
    //Array of newly created tasks
    $scope.tasks = [];
    //Car objects with defaults
    $scope.newCar = {
        type: "gas",
        make: "N/A",
        model: "N/A",
        year: 1900,
        odo: 0
        };
    
    $scope.create = function(){
        //Persist the new car object and get its id back
        $http.post(carUtil.getApiUrl() + '/api', $scope.newCar).
        then(function (response){
            console.log(response.data);
            var newCarId = response.data.id;
            $scope.tasks.forEach(function(task){
                //Use the ID to add all the created tasks to it
                task.date = carUtil.dateToString(task.raw_date);
                $http.post(carUtil.getApiUrl() + '/api/' + newCarId + '/task', task).
                then(function(response){
                    console.log(response.data);
                }, function(response){
                    console.log(response.data);
                    $window.alert("Could not add " + task.type
                    + "\n" + response.data.error);
                });
            });
        });
       $window.alert("Processed car");
    };
    $scope.addTask = function(){
        //Task object with default values
        //Use a Date objects so that dates can be more convenietly displayed in the form
        $scope.tasks.push({
            car_id: NaN,
            type: 'tire_rotation',
            cost: 0,
            raw_date: new Date(),
            date: ''
        });
    };
    $scope.removeTask = function(index){
        $scope.tasks.splice(index, 1);
    }
    $scope.back = function(){
        $window.location.href = 'car_app_main.html';
    }
});

//Show the details of a single car
app.controller('detailsController', function($scope, $window, $http, carUtil){
    selectionId = $window.sessionStorage.getItem('carSelection');
    $http.get(carUtil.getApiUrl() + '/api/' + selectionId).
    then(function(response) {
        $scope.car = response.data;
        console.log($scope.car);
    });
    $scope.back = function(){
        $window.location.href = 'car_app_main.html';
    };
});

app.controller('editController', function($scope, $window, $http, carUtil){
    selectionId = $window.sessionStorage.getItem('carSelection');
    //Array of newly creatad tasks
    //The existing tasks are in the response
    $scope.tasks = [];
    $http.get(carUtil.getApiUrl() + '/api/' + selectionId).
    then(function(response) {
        $scope.car = response.data;
        console.log($scope.car);
        $scope.car.maintenance_tasks.forEach(function(task){
            task.raw_date = carUtil.stringToDate(task.date);
            task.changed = false;
        });
    });
    $scope.edit = function(){
        console.log($scope.car);
        console.log($scope.tasks);
        //First, persist the changes to the car
        $http.put(carUtil.getApiUrl() + '/api/' + selectionId, $scope.car).
        then(function(response){
            console.log(response.data);
        });
        //Then, persist the modified tasks
        $scope.car.maintenance_tasks.forEach(function(task){
            if(task.changed){
                task.date = carUtil.dateToString(task.raw_date);
                $http.put(carUtil.getApiUrl() + '/api/' + selectionId + '/task/' + task.id, task).
                then(function(response){
                    console.log(response.data);
                });
            }
        });
        //Last, persist the newly created tasks
        $scope.tasks.forEach(function(task){
            task.date = carUtil.dateToString(task.raw_date);
            $http.post(carUtil.getApiUrl() + '/api/' + selectionId + '/task', task).
            then(function(response){
                console.log(response.data);
            }, function(response){
                console.log(response.data);
                    $window.alert("Could not add " + task.type
                    + "\n" + response.data.error);
            });
            
        });
    $window.alert("Processed changes");
    };
    $scope.createTask = function(){
        $scope.tasks.push({
            car_id: $scope.car.id,
            type: 'tire_rotation',
            cost: 0,
            raw_date: new Date(),
            date: ''
            })
    };
    
    //Keep track of which tasks are modified so
    //the unchanged ones won't be persisted again
    $scope.markChanged = function(index){
        $scope.car.maintenance_tasks[index].changed=true;
    };
    
    //Deletes are instantly persisted 
    $scope.deleteExistingTask = function(index){
        var taskToDelete = $scope.car.maintenance_tasks.splice(index, 1)[0];
        console.log(taskToDelete);
        $http.delete(carUtil.getApiUrl() + '/api/task/' + taskToDelete.id).
        then(function(response){
            console.log("DELETED");
        });
    }; 
    $scope.back = function(){
        $window.location.href = 'car_app_main.html';
    };
    
    //Remove a newly created task
    $scope.removeTask = function(index){
        $scope.tasks.splice(index, 1);
    };
});
